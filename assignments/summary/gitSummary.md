# Git

Git is version control software which makes work on team projects easy   

![git](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)

## Branch

* here main software is main Branch and the separate instances of the code are the branches.

## Commit

* this is same as saving work and it is saved on local machine until you push it

##  push

* this is synching your commits to gitlab

## Merge

* It is essentially intergrating two branches

## Clone

* It takes the entire repository and makes a copy of it.

## Fork

* It is similar to clone but it makes a copy under your own name