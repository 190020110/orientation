#GITLAB SUMMARY

* GITLAB is easy to use __Git Based Repository__ manager and it is used for software development.

* It helps a team to work on single project together and hence it is an efficient tool.

* It also provides functions such as wiki, issues, cloning.


![GITLAB IMAGE](https://learn.hibbittsdesign.org/user/pages/images/using-github-desktop-new-native-and-gitlab/copy-the-https-address-of-your-gitlab-project-repository.png)